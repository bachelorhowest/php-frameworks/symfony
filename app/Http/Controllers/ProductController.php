<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{

    public function all()
    {
        return new JsonResponse(Product::all());
    }
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function add(Request $request)
    {
        $product = new Product();
        $product->name = $request->name;
        $product->articleID = $request->articleId;
        $product->price = $request->price;
        $product->width = $request->width;
        $product->height = $request->height;
        $product->description = $request->description;
        $product->save();
        return new JsonResponse("succes");
    }
}
